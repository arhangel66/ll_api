import traceback
from urllib.parse import parse_qs

import requests
from django.http import HttpResponse
from django.views import View

from ll_api.connector.models import CrmLogs


class ConnectorView(View):
    def post(self, request, path):
        res = call(request.POST.dict(), path)
        return HttpResponse(res.get('raw', ''))

    def get(self, request, path):
        res = call(request.GET.dict(), path)
        return HttpResponse(res.get('raw', ''))



def call(data, path, log_id=None, prospect_id=None):
    # importing here, instead of module top, to avoid circular import
    result = {'is_error': False, 'res': {}, 'raw': ''}
    if data.get('username') == 'demoapi':
        domain = 'demoaws.limelightcrm.com'
    else:
        domain = 'oneclickawaytgc.limelightcrm.com'

    url = "https://%s/%s" % (domain, path)

    # save call to log
    save_data = CrmLogs.clean_data(data.copy())

    if log_id:
        log = CrmLogs.objects.get(id=log_id)
        log.send_text = save_data
        log.save()
    else:
        log = CrmLogs.objects.create(command_type=data.get('method'), send_text=save_data, url=url)

    try:
        resp = requests.post(url, data=data)
        result['raw'] = resp.content
        result['res'] = parse_qs(resp.content.decode('utf-8'))
        res2 = {}
        for key, val in result['res'].items():
            if val:
                res2[key] = val[0]
        result['res'] = res2

        if 'error_message' in result['res'] or 'errorMessage' in result['res'] or 'declineReason' in result['res']:
            result['is_error'] = True

        elif result['res'].get('errorFound', None) == ['1']:
            result['is_error'] = True

    except:
        result['is_error'] = True
        result['res'] = traceback.format_exc()

    save_dic = result['res']
    log.get_text = save_dic
    log.is_error = result['is_error']
    log.save()
    return result
