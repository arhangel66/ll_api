from django.contrib import admin

# class JsonForm(forms.ModelForm):
#     class Meta:
#         model = CrmLogs
#         fields = '__all__'
#         widgets = {
#             'get_text': PrettyJSONWidget(),
#             'send_text': PrettyJSONWidget(),
#         }
from ll_api.connector.models import CrmLogs


@admin.register(CrmLogs)
class CrmLogsAdmin(admin.ModelAdmin):
    search_fields = ("send_text", "get_text", )
    list_display = ('id', 'command_type', 'is_error', 'created', 'get_time')
    list_filter = ('url', 'is_error', 'command_type',)

    # form = JsonForm
