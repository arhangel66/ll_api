# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from ll_api.connector.models import CrmLogs


class Command(BaseCommand):
    help = "clean cvv"

    def handle(self, *args, **options):
        qsCrmLogs = CrmLogs.objects.all()
        for log in qsCrmLogs:
            log.clean_cvv()
