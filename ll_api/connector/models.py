import json
import time
from pprint import pprint

from django.conf import settings
from django.db import models
from django_extensions.db.models import TimeStampedModel


class CrmLogs(TimeStampedModel):
    command_type = models.CharField(null=True, blank=True, max_length=100)
    url = models.CharField(null=True, blank=True, max_length=255)
    # send_text = JSONField(null=True, blank=True)
    # get_text = JSONField(null=True, blank=True)
    send_text = models.TextField(null=True, blank=True)
    get_text = models.TextField(null=True, blank=True)

    is_error = models.NullBooleanField()

    # is_real = models.BooleanField(default=True)

    def __str__(self):
        return u"%s - %s - %s" % (self.id, self.url, self.modified)

    def get_time(self):
        length = (self.modified - self.created)
        return "%s.%s sec" % (length.seconds, length.microseconds // 100000)

    def get_res(self):
        """
        return result_dict
        :return:
        """
        try:
            result = json.loads(self.get_text.replace("'", '"'))
        except:
            result = self.get_text
        return result

    def get_send(self):
        try:
            result = json.loads(self.send_text.replace("'", '"'))
        except:
            result = self.send_text
        return result

    @classmethod
    def clean_data(cls, data):
        star_fields = ['password', 'cvv', 'CVV', 'expirationDate']
        for field in star_fields:
            if field in data:
                data[field] = '*' * len(data[field])

        if 'creditCardNumber' in data:
            data['creditCardNumber'] = '*' * (len(data['creditCardNumber']) - 4) \
                                            + data['creditCardNumber'][-4:]
        return data

    def clean_cvv(self):
        send_dict = self.get_send()
        if isinstance(send_dict, dict):
            send_dict = self.clean_data(send_dict)
            self.send_text = json.dumps(send_dict)
            print('log %s cleaned' % self.id)
            self.save()
        else:
            print('cant read json %s' % self.id)

    def is_finish(self):
        # TODO make waiting finish 5-7 sec
        for i in range(7):
            log = CrmLogs.objects.get(id=self.id)
            print(i, log.get_text)
            if log.get_text is not None:
                self.get_text = log.get_text
                self.is_error = log.is_error
                return True
            time.sleep(1)

        self.is_error = True
        self.get_text = 'Too long response. Please try again later'
        self.save()
        return True

